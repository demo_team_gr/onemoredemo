package com.msgkatz.onemoredemo.domain.interactors

import com.msgkatz.onemoredemo.domain.DataRepo
import com.msgkatz.onemoredemo.data.entities.Rate
import io.reactivex.Observable

/**
 * Simple use-case to subscribe smth with updates of currency rates
 */
class SubscribeRates constructor(private val repo: DataRepo)
    : BaseInteractor<List<Rate>, String>()
{
    override fun buildObservable(parameter: String): Observable<List<Rate>> {
        return repo.getRatesByBase(parameter)
    }

}