package com.msgkatz.onemoredemo.domain.interactors

import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers

abstract class BaseInteractor<Result, Param> {

    private val  disposables: CompositeDisposable = CompositeDisposable()

    protected abstract fun buildObservable(parameter: Param) : Observable<Result>

    fun execute(observer: DisposableObserver<Result>?, params: Param) {
        val observable = buildObservable(params)

        if (observer != null)
        {
            val disposable = observable
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(observer)

            this.disposables.add(disposable)
        }


    }

}