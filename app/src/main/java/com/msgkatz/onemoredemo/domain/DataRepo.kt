package com.msgkatz.onemoredemo.domain

import com.msgkatz.onemoredemo.data.entities.Rate
import io.reactivex.Observable

/**
 * Main interface to access App's Data
 */
interface DataRepo {

    /**
     * we're using rates only, so here they are
     */
    fun getRatesByBase(base: String): Observable<List<Rate>>
}