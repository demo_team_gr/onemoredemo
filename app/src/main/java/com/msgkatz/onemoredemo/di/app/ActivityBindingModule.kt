package com.msgkatz.onemoredemo.di.app

import com.msgkatz.onemoredemo.di.scope.ActivityScope
import com.msgkatz.onemoredemo.presentation.ui.main.MainActivity
import com.msgkatz.onemoredemo.presentation.ui.main.MainActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = arrayOf(MainActivityModule::class))
    abstract fun bindMainActivity(): MainActivity
}