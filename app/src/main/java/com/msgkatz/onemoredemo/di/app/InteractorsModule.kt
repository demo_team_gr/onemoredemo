package com.msgkatz.onemoredemo.di.app

import com.msgkatz.onemoredemo.domain.DataRepo
import com.msgkatz.onemoredemo.domain.interactors.SubscribeRates
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class InteractorsModule {

    @Provides
    @Singleton
    fun providesSubscribeRates(repo: DataRepo): SubscribeRates = SubscribeRates(repo)
}