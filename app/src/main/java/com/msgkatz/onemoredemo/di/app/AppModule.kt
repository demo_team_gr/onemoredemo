package com.msgkatz.onemoredemo.di.app

import com.msgkatz.onemoredemo.BuildConfig
import com.msgkatz.onemoredemo.data.net.CurrencyRatesApi
import com.msgkatz.onemoredemo.data.repo.DataRepoImpl
import com.msgkatz.onemoredemo.domain.DataRepo
import com.msgkatz.onemoredemo.presentation.common.messaging.IRxBus
import com.msgkatz.onemoredemo.presentation.common.messaging.RxBus
import com.msgkatz.onemoredemo.utils.BaseAssetStorer
import com.msgkatz.onemoredemo.utils.Parameters
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    fun providesDataRepo(dataRepo: DataRepoImpl) : DataRepo {
        return dataRepo
    }

    @Provides
    fun providesLoggingInterceptor(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor().apply {
            level = if (BuildConfig.DEBUG) {
                HttpLoggingInterceptor.Level.BODY
            } else {
                HttpLoggingInterceptor.Level.NONE
            }
        }
    }

    @Provides
    fun providesOkHttpClient(): OkHttpClient =
        OkHttpClient.Builder().addInterceptor(providesLoggingInterceptor()).build()

    @Provides
    fun providesRetrofit(
        okHttpClient: OkHttpClient
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(Parameters.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .build()
    }


    @Provides
    @Singleton
    fun providesRatesApi(retrofit: Retrofit) : CurrencyRatesApi {
        return retrofit.create(CurrencyRatesApi::class.java)!!
    }

    @Provides
    @Singleton
    fun providesBaseAssetStorer() : BaseAssetStorer =
            BaseAssetStorer()

    @Provides
    @Singleton
    fun providesUIMessaging(): IRxBus =
            RxBus()



}