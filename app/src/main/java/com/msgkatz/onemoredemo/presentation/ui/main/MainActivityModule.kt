package com.msgkatz.onemoredemo.presentation.ui.main

import android.app.Activity
import android.content.Context
import androidx.annotation.Nullable
import com.msgkatz.onemoredemo.di.scope.ActivityScope
import com.msgkatz.onemoredemo.presentation.common.mvp.BasePresenter
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainActivityModule {

    @Binds
    @ActivityScope
    internal abstract fun activityContext(activity: Activity): Context

    @Binds
    @ActivityScope
    abstract fun activity(mainActivity: MainActivity): Activity

    @Binds
    @ActivityScope
    internal abstract fun ratesPresenter(presenter: MainPresenter): MainContract.Presenter

    @Binds
    @ActivityScope
    internal abstract fun view(mainActivity: MainContract.View): MainContract.View




}