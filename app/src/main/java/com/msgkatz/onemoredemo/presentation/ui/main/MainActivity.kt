package com.msgkatz.onemoredemo.presentation.ui.main

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.msgkatz.onemoredemo.R
import com.msgkatz.onemoredemo.data.entities.Rate
import com.msgkatz.onemoredemo.presentation.common.activity.BaseActivity
import com.msgkatz.onemoredemo.presentation.common.Layout
import javax.inject.Inject

/**
 * Due to the super simplification MainActivity's used both as basic container and MVP's view
 */
@Layout(id = R.layout.activity_main)
class MainActivity : BaseActivity(), MainContract.View {

    @Inject
    lateinit var ratesAdapter: RatesAdapterAsync

    @Inject
    lateinit var presenter: MainContract.Presenter

    private lateinit var recyclerView: RecyclerView

    private val handler = Handler(Looper.getMainLooper())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        presenter.view = this
        setupRecycler()
    }

    override fun onStart() {
        super.onStart()
        presenter.start()

    }


    override fun onResume() {
        super.onResume()
    }

    override fun onStop() {
        super.onStop()
        presenter.stop()
    }

    private fun setupRecycler()
    {
        recyclerView = findViewById(R.id.rvlist)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = ratesAdapter

        val cb: RatesAdapterAsync.OnClickCallback = object:RatesAdapterAsync.OnClickCallback() {
            override fun onItemClicked(rate: Rate, position: Int) {
                presenter.processNewClick(rate)
            }
        }
        ratesAdapter.setOnClickCallback(cb)
    }

    override fun updateRates(rates: List<Rate>) {
        ratesAdapter.updateRates(rates)
    }

    override fun scrollToTop() {
        //recyclerView.smoothScrollToPosition(0)
        recyclerView.scrollToPosition(0)
    }


    override fun updateRatesWithScroll(rates: List<Rate>) {

        handler.postDelayed(object:Runnable {
            override fun run() {
                //recyclerView.smoothScrollToPosition(0)
                recyclerView.scrollToPosition(0)
            }
        }, 100)

        ratesAdapter.updateRates(rates)


    }


}
