package com.msgkatz.onemoredemo.presentation.ui.main

import com.msgkatz.onemoredemo.data.entities.Rate
import com.msgkatz.onemoredemo.presentation.common.mvp.BasePresenter
import com.msgkatz.onemoredemo.presentation.common.mvp.BaseView

/**
 * Simplified decoupling for ui
 * Yep, mvp's quite enough
 */
interface MainContract {

    interface View: BaseView {

        /**
         * Updates recycler's list with new incoming rates
         */
        fun updateRates(rates: List<Rate>)
        fun updateRatesWithScroll(rates: List<Rate>)

        fun scrollToTop()
    }

    interface Presenter: BasePresenter<View> {

        /**
         * Starts the process of rearrange of recycler's list items
         */
        fun processNewClick(newBase: Rate)
    }
}