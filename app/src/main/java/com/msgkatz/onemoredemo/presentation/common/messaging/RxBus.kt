package com.msgkatz.onemoredemo.presentation.common.messaging

import com.jakewharton.rxrelay2.PublishRelay
import com.jakewharton.rxrelay2.Relay
import io.reactivex.Observable

class RxBus: IRxBus {

    override fun send(event: Any) {
        _bus.accept(event)
    }

    override fun toObservable(): Observable<Any> {
        return _bus
    }

    override fun hasObservers(): Boolean {
        return _bus.hasObservers()
    }

    companion object {
        @JvmStatic val _bus: Relay<Any> = PublishRelay.create<Any>().toSerialized()
    }
}