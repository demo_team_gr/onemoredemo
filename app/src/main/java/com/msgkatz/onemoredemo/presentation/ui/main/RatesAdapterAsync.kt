package com.msgkatz.onemoredemo.presentation.ui.main

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.msgkatz.onemoredemo.R
import com.msgkatz.onemoredemo.data.entities.Rate
import com.msgkatz.onemoredemo.presentation.common.messaging.IRxBus
import com.msgkatz.onemoredemo.presentation.common.messaging.RefreshEvent
import com.msgkatz.onemoredemo.utils.BaseAssetStorer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.*
import java.math.RoundingMode
import java.text.DecimalFormat
import java.util.*
import javax.inject.Inject

/**
 * RatesAdapter holds rates list for recycler view
 * Receives rates updates from remote source via presenter's logic
 * Uses coroutines to calc diff in rates on each update
 * Uses IRxBus internally to refresh recycler's values when base asset's amount changes
 *
 */
class RatesAdapterAsync @Inject constructor(context: Context,
                                       var rxBus: IRxBus,
                                       var base: BaseAssetStorer
) : RecyclerView.Adapter<RatesAdapterAsync.RatesViewHolder>()
{

    abstract class OnClickCallback() {
        open fun onItemClicked(rate: Rate, position: Int) {}
    }

    private var rates: MutableList<Rate> = ArrayList<Rate>()

    private var callback: OnClickCallback? = null

    private val disposables = CompositeDisposable()

    private val pendingUpdates: ArrayDeque<List<Rate>> = ArrayDeque()
    private val handler = Handler(Looper.getMainLooper())
    private var isInitialised = false
    private val uiDispatcher: CoroutineDispatcher = Dispatchers.Main
    private val bgDispatcher: CoroutineDispatcher = Dispatchers.IO
    private val job = SupervisorJob()
    private val scope = CoroutineScope(uiDispatcher + job)

    private val df = DecimalFormat("#.####")

    init {
        df.roundingMode = RoundingMode.CEILING
    }

    public fun updateRates(newRates: List<Rate>) {
        if (!isInitialised)
        {
            this.rates.addAll(newRates)
            notifyDataSetChanged()
            isInitialised = true
        } else
        {
            pendingUpdates.add(newRates)
            if (pendingUpdates.size > 1)
                return

            scope.launch {
                updateInternal(newRates)
            }

        }
    }

    private suspend fun updateInternal(newRates: List<Rate>) {
        withContext(uiDispatcher) {
            val result = withContext(bgDispatcher) {
                val diffResult: DiffUtil.DiffResult = DiffUtil.calculateDiff(
                    RatesAdapterDiffCallback(
                        rates,
                        newRates
                    )
                )
                handler.post(object:Runnable {
                    override fun run() {
                        applyDiffResult(newRates, diffResult)
                    }
                })
            }
        }
    }

    private fun applyDiffResult(newRates: List<Rate>, diffResult: DiffUtil.DiffResult) {
        pendingUpdates.remove()
        diffResult.dispatchUpdatesTo(this)
        this.rates.clear()
        this.rates.addAll(newRates)

        if (pendingUpdates.size > 0)
        {
            scope.launch {
                updateInternal(pendingUpdates.peek())
            }
        }
    }

    fun setOnClickCallback(callback: OnClickCallback) {
        this.callback = callback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RatesViewHolder {
        val holder = RatesViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_rv_rate, parent, false)
        )

        holder.itemView.setOnClickListener {
            val position = holder.adapterPosition
            callback?.onItemClicked(rates.get(position), position)

            //holder.moveToTop()
            //moveToTop(position)
        }

        disposables.add(rxBus
            .toObservable()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { it ->
                if (it is RefreshEvent && holder.adapterPosition > 0) {
                    val position = holder.adapterPosition
                    var rate = rates.get(position).rate
                    holder.currencyRate.hint = df.format(base.baseAmount * rate)
                }
            })

        return holder
    }

    fun moveToTop(position: Int) {
        position.takeIf { it > 0 }?.also { currentPosition ->
            rates.removeAt(currentPosition).also {
                //rates.add(currentPosition - 1, it)
                rates.add(0, it)
            }
            //notifyItemMoved(currentPosition, currentPosition - 1)
            notifyItemMoved(currentPosition, 0)


        }
    }

    override fun getItemCount(): Int
            = rates.size


    override fun onBindViewHolder(holder: RatesViewHolder, position: Int) {
        val rate: Rate = rates.get(position)
        with (holder) {
            currencyName.text = rate.name
            currencyRate.hint = df.format(base.baseAmount * rate.rate)
            if (position == 0) {
                currencyRate.setEnabled(true)
                currencyRate.setText(df.format(base.baseAmount))
            } else {
                currencyRate.setEnabled(false)
                currencyRate.text.clear()
            }
        }
    }

    override fun onBindViewHolder(
        holder: RatesViewHolder,
        position: Int,
        partialChangePayloads: List<Any>
    ) {
        if (!partialChangePayloads.isEmpty()) {

            val o: Bundle = partialChangePayloads.get(0) as Bundle

            for (key: String in o.keySet())
                if (key.equals(RatesAdapterDiffCallback.KEY_DIFF))
                {
                    val values: DoubleArray? = o.getDoubleArray(RatesAdapterDiffCallback.KEY_DIFF)
                    if (values == null || (values.size != 2))
                        return

                    holder.currencyRate.hint = df.format(base.baseAmount * values.get(1))
                }

            if (position == 0) {
                holder.currencyRate.setEnabled(true)
                holder.currencyRate.setText(df.format(base.baseAmount))
            } else {
                holder.currencyRate.setEnabled(false)
                holder.currencyRate.text.clear()
            }

        } else {
            onBindViewHolder(holder, position)
        }
    }


    inner class RatesViewHolder(itemView: View,
                                var currencyName: TextView = itemView.findViewById(R.id.title_main),
        //var currencyRate: TextView = itemView.findViewById(R.id.price)
                                var currencyRate: EditText = itemView.findViewById(R.id.price)
    ) : RecyclerView.ViewHolder(itemView) {

        init {
            currencyRate.addTextChangedListener(object: TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                }

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    // here
                    if (!currencyRate.isEnabled)
                        return
                    var newAmount: Double = 0.0
                    if (s?.toString() != null && s?.toString().length > 0)
                        newAmount = s.toString().toDouble()
                    base.baseAmount = newAmount
                    rxBus.send(RefreshEvent())

                }
            })
        }

        public fun moveToTop(): (View) -> Unit = {
            layoutPosition.takeIf { it > 0 }?.also { currentPosition ->
                rates.removeAt(currentPosition).also {
                    //rates.add(currentPosition - 1, it)
                    rates.add(0, it)
                }
                //notifyItemMoved(currentPosition, currentPosition - 1)
                notifyItemMoved(currentPosition, 0)
            }
        }

        private fun remove(): (View) -> Unit = {
            layoutPosition.also { currentPosition ->
                rates.removeAt(currentPosition)
                notifyItemRemoved(currentPosition)
            }
        }
    }





}
