package com.msgkatz.onemoredemo.presentation.ui.main

import com.msgkatz.onemoredemo.data.entities.Rate
import com.msgkatz.onemoredemo.domain.interactors.SubscribeRates
import com.msgkatz.onemoredemo.utils.BaseAssetStorer
import com.msgkatz.onemoredemo.utils.Logs
import io.reactivex.observers.DisposableObserver
import javax.inject.Inject

/**
 * Main presenter is used for the following
 * 1) subscribes for currency rates update
 * 2) controls base asset and it's value
 * 3) controls and updates rates list for the view
 */
class MainPresenter @Inject constructor(var subRates: SubscribeRates,
                                        var base: BaseAssetStorer
) : MainContract.Presenter {

    override lateinit var view: MainContract.View
    private var observer: DisposableObserver<List<Rate>>? = null
    private var lastRates: MutableSet<Rate> = HashSet<Rate>()

    override fun start() {
        if (observer == null)
            init()

        subscribeRates()
    }

    override fun stop() {
        observer?.dispose()
    }

    private fun subscribeRates() {
        subRates.execute(observer, base.baseAsset)
    }

    private fun init() {
        observer = object:DisposableObserver<List<Rate>>() {
            override fun onError(e: Throwable) {
                Logs.s(this, "error occured=${e.message}")
            }

            override fun onNext(t: List<Rate>) {
                var innerList: MutableList<Rate> = ArrayList<Rate>()

                if (t.size > 0) {
                    lastRates.clear()
                    lastRates.addAll(t)
                }
                innerList.add(Rate(base.baseAsset, 1.0))
                innerList.addAll(t)

                view.updateRates(innerList)

                var sb: StringBuilder = StringBuilder()
                for (r in t) {
                    sb.append("${r.name}=${r.rate};")
                }
                Logs.d(this, sb.toString())
            }

            override fun onComplete() {
                Logs.s(this, "completed...")
            }

        }
    }

    override fun processNewClick(newBase: Rate) {
        if (!newBase.name.equals(base.baseAsset))
        {
            Logs.d(this, "asset=${newBase.name} was clicked...")
            observer?.dispose()


            var innerList: MutableList<Rate> = ArrayList<Rate>()

            for (r in lastRates)
            {
                if (!r.name.equals(newBase.name))
                    innerList.add(Rate(r.name, r.rate/newBase.rate))
            }
            innerList.add(Rate(base.baseAsset, 1.0/newBase.rate))
            innerList.sortWith(compareBy({it.name}))

            base.baseAsset = newBase.name
            innerList.add(0, Rate(base.baseAsset, 1.0))

            base.baseAmount = (base.baseAmount * newBase.rate)

            view.updateRatesWithScroll(innerList)

            init()
            subscribeRates()

        }
    }

}