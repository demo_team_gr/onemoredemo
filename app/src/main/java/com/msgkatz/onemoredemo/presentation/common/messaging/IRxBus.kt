package com.msgkatz.onemoredemo.presentation.common.messaging

import io.reactivex.Observable

/**
 * Created by msgkatz on 23/05/17.
 *
 * Lightweight queue to communicate between UI layer parts
 */
interface IRxBus {
    fun send(event: Any)
    fun toObservable(): Observable<Any>
    fun hasObservers(): Boolean
}