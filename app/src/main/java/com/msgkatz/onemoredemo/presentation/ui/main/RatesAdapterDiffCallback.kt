package com.msgkatz.onemoredemo.presentation.ui.main

import android.os.Bundle
import androidx.recyclerview.widget.DiffUtil
import com.msgkatz.onemoredemo.data.entities.Rate

class RatesAdapterDiffCallback(var ratesOld: List<Rate>,
                               var ratesNew: List<Rate>
) : DiffUtil.Callback()
{
    override fun getOldListSize(): Int {
        return ratesOld.size
    }

    override fun getNewListSize(): Int {
        return ratesNew.size
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return ratesOld.get(oldItemPosition).equals(ratesNew.get(newItemPosition))
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return ratesOld.get(oldItemPosition).rate == ratesNew.get(newItemPosition).rate
    }

    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
        val rateNew: Rate = ratesNew.get(newItemPosition)
        val rateOld: Rate = ratesOld.get(oldItemPosition)

        val diffBundle: Bundle = Bundle()
        if (rateNew.rate != rateOld.rate)
        {
            diffBundle.putDoubleArray(KEY_DIFF, doubleArrayOf(rateOld.rate, rateNew.rate))
        }

        if (diffBundle.size() == 0) return null
        return diffBundle
    }

    companion object {
        const val KEY_DIFF: String = "KEY_DIFF"
    }
}