package com.msgkatz.onemoredemo.utils

/**
 * Data class to store current base asset and it's amount
 * Amount updated via user input
 */
data class BaseAssetStorer(var baseAsset: String = Parameters.BASE_ASSET,
                           var baseAmount: Double = 1.0)
