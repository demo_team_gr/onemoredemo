package com.msgkatz.onemoredemo.utils

class Parameters {
    companion object {
        @JvmStatic val BASE_URL = "https://revolut.duckdns.org/"

        @JvmStatic val DEBUG = true

        const val BASE_ASSET = "USD"
    }
}
