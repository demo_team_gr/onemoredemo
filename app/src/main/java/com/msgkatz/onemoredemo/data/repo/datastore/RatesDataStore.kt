package com.msgkatz.onemoredemo.data.repo.datastore

import com.msgkatz.onemoredemo.data.net.CurrencyRatesApi
import com.msgkatz.onemoredemo.data.entities.Rate
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RatesDataStore @Inject constructor (
    private val ratesApi: CurrencyRatesApi
) {

    private val rateHolder: RateHolder = RateHolder(ratesApi)

    fun getRatesByBase(base: String) : Observable<List<Rate>>
    {
        return rateHolder.subscribeDataUpdate(base)
    }

}