package com.msgkatz.onemoredemo.data.repo.datastore

import com.google.gson.JsonObject
import com.jakewharton.rxrelay2.PublishRelay
import com.msgkatz.onemoredemo.data.entities.BaseRatesDT
import com.msgkatz.onemoredemo.data.net.CurrencyRatesApi
import com.msgkatz.onemoredemo.data.entities.Rate
import com.msgkatz.onemoredemo.data.entities.mappers.RateDataMapper
import com.msgkatz.onemoredemo.utils.Logs
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import retrofit2.Response
import java.util.concurrent.*

/**
 * Inner datastore class to provide rates-related data
 * Exposes PublishRelay observable to subscribe for rates-related data updates
 * Updates rates internally via remote API and pushes updates
 */
class RateHolder constructor(var ratesApi: CurrencyRatesApi)
{
    private val relayBase: PublishRelay<List<Rate>> = PublishRelay.create()
    private val disposables: CompositeDisposable = CompositeDisposable()
    private var observer: DisposableObserver<Response<BaseRatesDT>>? = null

    fun subscribeDataUpdate(base: String) : Observable<List<Rate>>
    {
        disposables.clear()
        initUpdate(base)
        return relayBase
    }

    private fun initUpdate(base: String)
    {
        initSubscriptionController()
        val disposable = Schedulers.newThread().createWorker().schedulePeriodically({ initStream(base) }, 0, 1000, TimeUnit.MILLISECONDS)
        disposables.add(disposable)

    }

    private fun initStream(base: String)
    {

        observer = initObserver()
        val disposable = ratesApi.getRates(base)
            .subscribeOn(Schedulers.computation())
            .observeOn(Schedulers.newThread())
            .subscribeWith(observer)

        disposables.add(disposable as Disposable)
    }

    private fun initObserver() =
        object : DisposableObserver<Response<BaseRatesDT>>() {
            override fun onError(e: Throwable) {
                Logs.s(this, "error occured=${e.message}")
            }

            override fun onNext(t: Response<BaseRatesDT>) {

                if (t.isSuccessful)
                    processData(t.body())
                else
                    processData(null)
            }

            override fun onComplete() {
                Logs.d(this, "completed...")
            }

        }



    private fun initSubscriptionController()
    {
        // TODO add extra scheduled disposable to control PublishRelay active subscriptions. If none - stop scheduled updater
    }

    private fun processData(input : BaseRatesDT?): List<Rate>?
    {
        val retVal: List<Rate> =  RateDataMapper.transform(input)

        if (retVal.size > 0)
            relayBase.accept(retVal)

        return retVal

    }






}