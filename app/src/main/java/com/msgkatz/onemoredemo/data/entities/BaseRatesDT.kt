package com.msgkatz.onemoredemo.data.entities

import com.google.gson.JsonObject
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Internal datalayer's entity
 * helps with loading and processing currency rates from remote API
 */
class BaseRatesDT {

    @SerializedName("base")
    @Expose
    var baseCurrency: String = ""

    @SerializedName("date")
    @Expose
    var baseDate: String = ""

    @SerializedName("rates")
    @Expose
    var rates: JsonObject? = null
}