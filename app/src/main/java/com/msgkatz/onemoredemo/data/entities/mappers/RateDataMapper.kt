package com.msgkatz.onemoredemo.data.entities.mappers

import com.google.gson.JsonObject
import com.msgkatz.onemoredemo.data.entities.BaseRatesDT
import com.msgkatz.onemoredemo.data.entities.Rate
import com.msgkatz.onemoredemo.data.repo.datastore.RateHolder

class RateDataMapper {

    companion object {


        fun transform(baseRatesDT: BaseRatesDT?): List<Rate> {

            if (baseRatesDT == null || baseRatesDT.rates == null)
                return emptyList<Rate>()

            return parseJsonObject(baseRatesDT.rates)

        }

        private fun parseJsonObject(input: JsonObject?): List<Rate> {

            var rateSet: MutableSet<Rate> = HashSet<Rate>()

            if (input == null)
                return rateSet.toList()

            // input.keySet()
            for (s in input.keySet()) {
                val rate: Double? = input.get(s).asDouble
                if (rate != null) {
                    rateSet.add(Rate(s, rate))
                }
            }

            return rateSet.toList().sortedWith(compareBy({ it.name }))
        }

        private fun <T> compareBy(vararg selectors: (T) -> Comparable<*>?): Comparator<T> {
            return object : Comparator<T> {
                public override fun compare(a: T, b: T): Int = compareValuesBy(a, b, *selectors)
            }
        }

    }
}