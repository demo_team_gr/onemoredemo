package com.msgkatz.onemoredemo.data.net

import com.msgkatz.onemoredemo.data.entities.BaseRatesDT
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Retrofit Service
 *
 * https://revolut.duckdns.org/latest?base=EUR
 */
interface CurrencyRatesApi {

    @GET("latest?")
    fun getRates(@Query("base") baseAsset: String) : Observable<Response<BaseRatesDT>>
}