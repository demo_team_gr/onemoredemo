package com.msgkatz.onemoredemo.data.entities

/**
 * Entity for storing currency asset by it's name and currency rate
 */
data class Rate(val name: String, val rate: Double) : Comparable<Rate> {

    override fun equals(other: Any?): Boolean {
        if (other == null)
            return false

        if (other !is Rate)
            return false

        return name.equals(other.name)
    }

    override fun hashCode(): Int {
        return name.hashCode()
    }

    override fun compareTo(other: Rate): Int {
        return name.compareTo(other.name)
    }
}