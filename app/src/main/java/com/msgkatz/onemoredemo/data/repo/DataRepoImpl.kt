package com.msgkatz.onemoredemo.data.repo

import com.msgkatz.onemoredemo.data.repo.datastore.RatesDataStore
import com.msgkatz.onemoredemo.domain.DataRepo
import com.msgkatz.onemoredemo.data.entities.Rate
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DataRepoImpl @Inject constructor (
    private val ratesDataStore: RatesDataStore
) : DataRepo
{
    override fun getRatesByBase(base: String): Observable<List<Rate>> {
        return ratesDataStore.getRatesByBase(base)
    }

}