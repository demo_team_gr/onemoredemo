package com.msgkatz.onemoredemo.data.domain.interactors

import com.msgkatz.onemoredemo.data.entities.Rate
import com.msgkatz.onemoredemo.domain.DataRepo
import com.msgkatz.onemoredemo.domain.interactors.SubscribeRates
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import org.mockito.ArgumentMatchers.startsWith

class SubscribeRatesTest {

    private val baseRate: String = "USD"
    private val repo: DataRepo = mock()
    private val subscribeRates: SubscribeRates = SubscribeRates(repo)
    private val data = listOf(Rate("AUD",1.6203), Rate("BGN", 1.9606))


    @Test
    fun testForCorrectThread() {

        whenever(repo.getRatesByBase(any())).thenReturn(Observable.just(data))

        val testobs: TestObserver<List<Rate>> = TestObserver()

        repo.getRatesByBase(baseRate).subscribe(testobs)

        testobs.awaitCount(1)

        val observingThreadName: String = testobs.lastThread().name
        assert(observingThreadName.startsWith("main"))
    }

}