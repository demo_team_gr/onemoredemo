package com.msgkatz.onemoredemo.data.repo.datastore

import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.msgkatz.onemoredemo.data.entities.BaseRatesDT
import com.msgkatz.onemoredemo.data.entities.Rate
import com.msgkatz.onemoredemo.data.net.CurrencyRatesApi
import com.msgkatz.onemoredemo.utils.Logs
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Observable
import io.reactivex.observers.DisposableObserver
import io.reactivex.observers.TestObserver
import io.reactivex.subscribers.TestSubscriber
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertTrue
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.junit.Test
import retrofit2.Response
import com.google.gson.JsonParser



class RateHolderTest {

    private val baseRate: String = "USD"
    private val restApi: CurrencyRatesApi = mock()
    private val rateHolder: RateHolder = RateHolder(restApi)

    private val baseRatesDT = BaseRatesDT()
    val errorResponseBody = ResponseBody.create(MediaType.parse(""), "Error")

    private fun fulfillBaseRate() {
        baseRatesDT.baseCurrency = baseRate
        baseRatesDT.baseDate = "2018-09-06"
        baseRatesDT.rates = JsonObject()

        val parser = JsonParser()
        val o = parser.parse("{\"AUD\":1.6203,\"BGN\":1.9606,\"BRL\":4.8035,\"CAD\":1.5375,\"CHF\":1.1302,\"CNY\":7.9644,\"CZK\":25.778,\"DKK\":7.4749,\"GBP\":0.90043,\"HKD\":9.1546,\"HRK\":7.4522,\"HUF\":327.28,\"IDR\":17366.0,\"ILS\":4.1808,\"INR\":83.921,\"ISK\":128.11,\"JPY\":129.87,\"KRW\":1307.9,\"MXN\":22.42,\"MYR\":4.8237,\"NOK\":9.7998,\"NZD\":1.7676,\"PHP\":62.744,\"PLN\":4.3288,\"RON\":4.6498,\"RUB\":79.768,\"SEK\":10.617,\"SGD\":1.6039,\"THB\":38.223,\"TRY\":7.6468,\"USD\":1.1662,\"ZAR\":17.867}").asJsonObject
        baseRatesDT.rates = o

    }

    @Test
    fun testUpdateError() {
        val response = Response.error<BaseRatesDT>(404, errorResponseBody)
        whenever(restApi.getRates(any())).thenReturn(Observable.just(response))

        val testobs: TestObserver<List<Rate>> = TestObserver()

        rateHolder.subscribeDataUpdate(baseRate).subscribe(testobs)

        testobs.awaitCount(1)

        testobs.assertNoErrors().assertNoValues()

    }

    @Test
    fun testUpdateOk() {
        fulfillBaseRate()
        val response = Response.success(baseRatesDT)
        whenever(restApi.getRates(any())).thenReturn(Observable.just(response))

        val testobs: TestObserver<List<Rate>> = TestObserver()

        rateHolder.subscribeDataUpdate(baseRate).subscribe(testobs)

        testobs.awaitCount(1)

        testobs.assertNoErrors().assertValueCount(1).assertValue { it -> it.size == 32 }

    }
}